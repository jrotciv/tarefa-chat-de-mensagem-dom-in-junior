function adicionarMensagem(){
    let input = document.querySelector(".caixa-mensagem")
    console.log("clicou")

    let divisao = document.createElement("div")
    let mensagem = document.createElement("p")
    let divisaoBotoes = document.createElement("div")
    let bEditar = document.createElement("button")
    let bApagar = document.createElement("button")

    divisao.classList.add("mensagem-enviada")
    mensagem.classList.add("mensagem")
    divisaoBotoes.classList.add("botoes")
    bEditar.classList.add("editar")
    bApagar.classList.add("apagar")

    mensagem.innerText = input.value
    bEditar.innerText = "Editar"
    bApagar.innerText = "Apagar"

    bApagar.addEventListener("click", () => apagarMensagem(divisao))

    divisaoBotoes.append(bEditar)
    divisaoBotoes.append(bApagar)
    divisao.append(mensagem)
    divisao.append(divisaoBotoes)

    let historico = document.querySelector(".historico")
    historico.append(divisao)
    input.value = ""
}

function apagarMensagem(mensagem){
    mensagem.parentNode.removeChild(mensagem)
}

let bEnviar = document.querySelector(".botao-enviar")
bEnviar.addEventListener("click", adicionarMensagem)